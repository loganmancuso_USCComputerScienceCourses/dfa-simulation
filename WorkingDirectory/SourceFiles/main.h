/****************************************************************
 * 'main.h'
 * Header file that includes the 'Utilities' code.
 *
 * Author/CopyRight: Mancuso, Logan
 * Last Edit Date: 11-27-2018--13:30:41
 *
**/

#ifndef MAIN_H
#define MAIN_H

#include <iostream>

#include "../../Utilities/utils.h"
#include "../../Utilities/scanner.h"

/**
 * Add Code Here:
**/

#include "simulation.h"

#endif //MAIN_H

/****************************************************************
 * End 'main.h'
**/
