# /****************************************************************
#  * 'Run.sh'
#  *
#  * Author/CopyRight: Mancuso, Logan
#  * Last Edit Date: 11-27-2018--13:31:08
#  *
#  * This script will run the program, run from
#  * WorkingDirectory/SourceFiles/
# **/

#!bin/bash

echo "Executing zeExecute.sh" 
echo "-----------------------------------------------------------------" 
../../.Executable/./zeExecute.sh $1 $2 $3
echo "Program Done Executing"

# /****************************************************************
#  * End 'Run.sh'
# **/